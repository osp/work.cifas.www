# work.cifas.www

Website for Cifas

## How to setup for content only editing

1. Install **Nextcloud client**, on Ubuntu you can

        sudo apt install nextcloud-desktop

2. Create a folder that is going to contain the content of the website, name it how you want to.
3. Using the **Nextcloud client**, set up a synchronization between the `/website/www` folder on <https://cloud.cifas.be> and the folder you created on your computer.

Now any modifications you will do in this *cloud-synched folder* will spread to the others, and reciprocally. The updates are detected and applied automatically by the **Nextcloud client**.

## How to set up for dev editing

### Install Pelican

1. install **pip** `sudo apt install python3-pip`
2. then use **pip** to install **pelican** `python3 -m pip install pelican[markdown]`

### Point content to the *cloud-synched folder*

1. First, do the same steps as for *content only editing*.
2. Then `git clone` this repo in a folder on your computer.
3. Go into the repo.
 
        cd work.cifas.wwww

4. Create a **symlink** named `content` that points to the *cloud-synched folder*. Depending on where you did put the *cloud-synched folder* folder relatively, it can look like this:

        ln -s ../cloud.cifas/www/ content

Now **Pelican** is going to use the *cloud-synched folder*, as it's **content**, to generate the website.

### Set up the output folder

The **Pelican** template will try to generate the website on your computer, here `/var/www/cifas`. It is so because the deployment works like that on the VPS server.

1. First, create the output directory `/var/www/cifas`.

        cd /var/www
        mkdir cifas
        
2. Make sure **Pelican** can write and access it.
        
        sudo chmod -R 755 cifas
        sudo chown -R user:www-data cifas

### Generate website locally

To launch Pelican locally and test the website on your computer.

1. Go into your repo `work.cifas.www` folder and launch a local server with
 
        pelican --listen

2. Generate the website locally 

        pelican

3. Access <http://localhost:8000> in your browser

It should open your local copy of the website.
If you edit anything, either the content in the *cloud-synched folder*, or the **Pelican** files in the repo, you have to `pelican` again to re-generate it.


## Deploy online

To deploy the online version

1. Open <http://deploy.cifas.be> in your browser
2. Click on the following 3 buttons in this order: `ingredient`, `recipe`, `bake` and wait for each action to finish. This will 
   1. Update the *cloud-synched folder* on the VPS, so it's in synch with everyone (get the new edited content)
   2. Pull the new commits done on the repository
   3. Regenerate the website using **Pelican** on the VPS
3. Open <http://proto.cifas.be> in your browser, the new version should be there.
