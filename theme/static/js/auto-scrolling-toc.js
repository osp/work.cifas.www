

function isInViewport(element) {
    const rect = element.getBoundingClientRect()
    return (
        rect.top >= -1 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)
    )
}

const events = document.querySelectorAll('.event')
const toc_links = document.querySelectorAll('.calendar__toc li');


function setup_toc_selection(){

    function update_selected(){
        // for every event
        for( let event of events){
    
            // if we have one element in viewport
            // state possible: 
            // * in between two - none are in vp
            // * one is in vp
            // * two are in vp
    
            if (isInViewport(event)) {
    
                // window.location.href = '#' + event.id;
                // let new_url = '#' + event.id;
                // window.history.pushState("", "", new_url);
    
                // reset all toc_links
                for(let tl of toc_links){
                    tl.classList.remove('active');
                }
    
                // active this one
                let toc_link = document.getElementById('toc__' + event.id);
                toc_link.classList.add('active');
            }
        }
    }
    
    // everytime we scroll
    window.addEventListener('scroll', update_selected);
    update_selected();
}