let condensed_class = 'pseudo-condensed';
let max_w;
let condensed_finished = false;

// PSEUDO CONDENSED TITLES
// -------------------------------------------------------------------------

function get_max_w(title){
    let test_event = title.closest('.title-wrapper');
    let max_w = test_event.offsetWidth;
    // console.log('max condensed title width', max_w);
    return max_w;
}

function fit_titles(){
    let titles = document.getElementsByClassName(condensed_class);
    for (let title of titles){
        let max_w = get_max_w(title);
        title.style.transform = "";
        let w = title.offsetWidth;
        if( w > max_w){
            let f = max_w / w;
            title.style.transform = 'scaleX(' + f + ')';
        }

        title.classList.add('computed');

    }
}

function setup_titles(callback){
    
    // we have to ensure that the font as loaded to get title sizes
    if(!condensed_finished){

        document.fonts.load('4rem "QTAntiqueOlive"').then(function(){

            setTimeout(function(){
                fit_titles();
                condensed_finished = true;
            }, 100);

            if(callback){
                callback();
            }
        })
    }
    else{
        fit_titles();
    }
}

window.addEventListener('resize', function(){
    setup_titles();
});