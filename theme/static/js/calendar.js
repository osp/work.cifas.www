let now = new Date();
let calendar = document.getElementsByClassName('calendar__events')[0];
let toc = document.getElementsByClassName('toc__list')[0];

let event_class = 'event';
let program_class = 'event__program';
let subevent_class = 'subevent';

// SORT CALENDAR
// -------------------------------------------------------------------------

function sort_events(container, $events, sort_toc=false){

    let events_list = []

    for (let $event of $events){

        // get the associated TOC link
        let $toc_link;
        if(sort_toc){
            $toc_link = document.getElementById('toc__' + $event.id);
            console.log($event.id);
            console.log($toc_link);
        }
        // and group it into an object
        let events_list_item = {
            event: $event,
            toc_link: $toc_link
        }
        events_list.push(events_list_item);

        // get back time data
        let dates = Array.from($event.querySelectorAll('.block__dates time')).map(function(x){
            // every element is a tuple of the DOM time element and it's javascript date object
            return [x, Date.parse(x.getAttribute('datetime'))];
        });

        // iterate on date and sort between the past and to come
        let next_dates = [];
        for(let d of dates){
            if (d[1] < now){
                // past dates
                d[0].classList.add('past');
            }
            else{
                next_dates.push(d);
            }
        }

        // get the next date to come, or last one if past
        let next_date;
        if(next_dates.length != 0){
            next_date = next_dates[0];
        }
        else{
            // past events
            $event.classList.add('past');
            if(sort_toc){
                $toc_link.classList.add('past');
            }
            next_date = dates[0];
        }

        // we're going to sort them according to that new attribute
        $event.setAttribute('data-next_date', next_date[1]);
    }

    // sort event by their next date
    // keeping past event on top
    // let sorted_event = Array.from(events);
    events_list.sort((a, b) => { return a.event.dataset.next_date - b.event.dataset.next_date});
    console.log('Sorted list', events_list);

    if (sort_toc){
        events_list.reverse();
    }

    // remove all event from calendar and TOC
    events_list.forEach(function(e){
        e.event.parentNode.removeChild(e.event);
        if(sort_toc){
            e.toc_link.parentNode.removeChild(e.toc_link);
        }
    });

    // add them back sorted
    events_list.forEach(function(e){
        container.appendChild(e.event);
        if(sort_toc){
            toc.appendChild(e.toc_link);
        }
    });
}


// MAIN
// -------------------------------------------------------------------------

function sort_program(program_container = document){
    // sort subevents
    let programs = program_container.querySelectorAll('.' + program_class + ' ul');
    
    for(let program of programs){
        let subevents = program.querySelectorAll('.' + subevent_class);
        sort_events(program, subevents, false);
    }
}

function setup_calendar(callback){

    // sort event
    let events = calendar.getElementsByClassName(event_class);
    sort_events(calendar, events, true);

    // sort subevents
    for (let event of events){
        sort_program(event);
    }

    calendar.classList.add('sorted');

    if(callback){
        callback();
    }
}

// prevent anchor scrolling
if (location.hash) {
    location.hash = '';
    setTimeout(function() {
      window.scrollTo(0, 0);
    }, 1);
}