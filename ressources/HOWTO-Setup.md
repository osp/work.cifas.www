
# Configuration pour éditer cifas.be

Ce document explique comment
1. Configurer un ordinateur pour éditer le contenu.
2. Deployer le site web, après l'avoir édité.
3. Installer Zettlr, un logiciel libre pour éditer du markdown et du yaml.


## Principe

Le contenu du site web est celui du dossier `website/www/` sur le **Nextcloud** <https://cloud.cifas.be>.

Ceci permet à chaque personne d'éditer le site web directement via des logiciels sur son propre ordinateur, dans un dossier automatiquement synchronisé avec les autres utilisateur·ices (via le **client de synchronisation Nextcloud**).


## Configuration Nextcloud

Si l'on veut éditer le contenu du site web sur son ordinateur, il faut mettre en place une petite configuration en utilisant le **client de synchronisation Nextcloud**.

1. Installer le **client de synchronisation Nextcloud**.
2. Créer un dossier sur son ordinateur, qui va contenir le contenu du site web, et le nommer comme on veut.
3. En utilisant le **client de synchronisation Nextcloud**, créer une nouvelle synchronisation entre le dossier `/website/www` sur <https://cloud.cifas.be> et le dossier qu'on a créer sur notre ordinateur.
   1. Ouvrir les paramètres du client
   2. `ajouter un nouveau compte` (si on a pas encore de compte lié à <https://cloud.cifas.be>)
   3. `ajouter une synchronisation de dossier`
   4. Sélectionner le dossier sur notre ordinateur, puis celui `website/www` sur le nuage.

Note: On peut bien sûr sélectionner d'autres dossiers à synchroniser en plus de celui du site web. Il faut cependant faire attention à ne pas copier plusieurs gigas sur notre ordinateur, sauf si cela est nécéssaire.

Maintenant toutes les modifications qu'on va faire dans ce dossier sur notre ordinateur se répercuteront chez les autres, et sur <https://cloud.cifas.be>, et réciproquement. Les updates sont prises en charge automatiquement à chaque modification d'un fichier par le **client de synchronisation Nextcloud**.


## Déploiement du site

Après avoir éditer des pages du site dans le dossier sur notre ordinateur, il faut re-déployer le site web pour y intégrer nos modifications sur le serveur.

1. Ouvrir <https://deploy.cifas.be> dans le navigateur
2. Cliquer sur les trois boutons dans l'ordre et attendre que chaque étapes se finissent.
   1. **Ingredients**: update le dossier synchronisé avec Nextcloud sur le serveur pour prendre nos modifications de contenu.
   2. **Recipe**: update les modifications de design et de structure (si nécessaire)
   3. **Bake**: regénère le site web avec nos modifications sur le serveur.
3. Ouvrir <https://proto.cifas.be> dans le navigateur pour voir la nouvelle version.


## Encodage dans Zettlr

[Zettlr](https://www.zettlr.com/) est un logiciel libre qui permet de prendre des notes en markdown.

après installation de zettler on doit setup l'espace de travail.

1. cliquer sur *fichier > ouvrir un espace de travail*, et selectionner le dossier `www` du nuage. Cela ouvre l'entièreté du l'arborescence des fichiers de contenu du site dans zettler.
2. cliquer sur *fichier > préférences > préférences*, et dans l'onglet *general* selectionner
   1. sous *mode gestionnaire de fichiers*, l'option *combiné*
   2. sous *afficher les fichiers seulement*, l'option *nom de fichier seulement*
   3. cocher la checkbox *afficher les extensions de fichier markdown*
3. ensuite dans l'onglet *afficher*
   1. cocher la checkbox *afficher les images*