# Markdown editor

criteria:
* FLOSS
* Windows, mac, linux compatible
* arborescence vue
* 2 modes (plain text + computed)
* update file in editor when different people have it open
* bonnus: custom CSS sheet
* bonnus: yaml front matter


Zettlr semble parfais: bonne doc, bien référencé, utiliser par plusieurs universitée. Le seul truc embêtant c'est qu'il propose une unique vue d'highlighted markdown.

Zettlr use the title frontmatter to title the file! (nice to see if every file has a title with pelican !)

L'avantage de Marktext est d'avoir une vue en source code en plus. Mais marktext fait disparaître la syntax completement dans la vue highlighted, plutot une vue rendu donc.


## Zettlr
https://www.zettlr.com/

* ✓ FLOSS (GNU public license)
* ✓ Windows, mac, linux compatible
* ✓ vue en arborescence
* ✗ seulement une vue avec highlight markdown
* ✓ update file in editor (it even ask confirmation)
* ✗ bonnus: custom CSS sheet
* ✓ bonnus: yaml front matter

## marktext
https://github.com/marktext/marktext

* ✓ FLOSS (MIT license)
* ✓ Windows, mac, linux compatible
* ✓ vue en arborescence
* ✓ 2 vues source code and highlighted markdown ! (but not at the same time)
* ✓ update file in editor (ask confirmation!)
* ✗ bonnus: custom CSS sheet (not already)
* ✓ bonnus: yaml front matter

## VSCodium
https://vscodium.com/#install

install process:
    1. install
    2. open a folder that's it

* ✓ FLOSS (MIT license)
* ✓ Windows, mac, linux compatible
* ✓ vue en arborescence
* ✓ 2 vues source code and highlighted markdown at the same time
* ✓ update file in editor (ask confirmation!)
* ✓ bonnus: custom CSS sheet (not already)
* ✓ bonnus: yaml front matter

---

eliminated:

## Joplin
https://joplinapp.org/

* ✗ doesn't show index.md file
* ✗ doesn't support yaml frontmatter
* ✗✗✗✗✗✗✗✗

## Ghostwriter
https://wereturtle.github.io/ghostwriter/

* ✓ FLOSS (GNU public license)
* ✗ " you have to compile it for MacOS (unofficial support provided by the community)"
* ✗ vue en arborescence
* ✗ 2 vues (highlighted markdown + html) mais pas de plain text 
* ✓ update file in editor (ask confirmation!)
* ? bonnus: custom CSS sheet
* ✗ bonnus: yaml front matter