
* **date structure for events**
* **allow media folder (images, etc)**
<!-- * sub-events systems -->

## for them to do
* choose 2 words for 'calendar' & 'library' 
* Pierre? peertube

## Structure
more urgent:
* rewrite url (we don't want the user to believe they can remove part of it and then don't end up on an index?)
* allow links between article (the url have to not change, see how does it works in typora)

less urgent:
* image process for not too big media
* typogriphy for smart typography
* translation principle (true fr / en website)
* a 404 page

## visual design
* a layout for the home
* a layout for the calendar
* a layout for the library

## CSS
* check open type css render properties (numeral, text lisibility, etc) and what's in the antque olive
* typo composition: drop a reset and write font-size and spacing
* 3 separate hyperlink design: 1 for _to liquid_, 1 for _to solid_, 1 for external (_the air_) ? 

## Deploy page
* better logs