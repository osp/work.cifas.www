
# Encoder du contenu sur cifas.be

Ce document explique
1. la structure du contenu du site web
2. comment encoder le contenu du site web

## Arborescence

Le contenu du site web est celui du dossier `website/www/` sur le **Nextcloud** <https://cloud.cifas.be>.

L'arborescence de sous-dossiers détermine différents paramètres de la structure du site.

## Meta-données en YAML

Chaque fichier commence par une _frontmatter_ écrite en YAML.
La _frontmatter_ est délimité par des `---`, en haut et en bas, pour la commencer et la finir.

**YAML est un language qui sert à encoder des données, facilement lisibles et écris par des humains.**

Cela fonctionne avec des paires de _clés_ et de _valeur_ séparé par un `:`.

La clé `title` est la clé obligatoire.

On doit donc avoir au minimum, au début de chaque page:

```YAML
---
title: titre de la page
---
```

Les lignes qui commence par des `#` sont des commentaires, on peut donc écrire ce que l'on veut dedans, ou caché une info en brouillon, qui ne sera pas prise en compte à la génération du site.

```YAML
---
title: titre de la page
# autre idée de titre: deuxième titre
---
```

### Note sur les guillemets

Les valeurs peuvent être encadrées par des guillemets simples (`'`) ou double (`"`) mais dans la plupart des cas ceux-ci ne sont pas nécéssaire.
Si une valeurs contient des symboles spéciaux qui veulent dire quelque chose en YAML, notamment des `:`, alors encadrer la valeur par des guillemets simple (`'`) est nécéssaire.

```YAML
---
title: 'edition 1: titre'
---
```

Il est possible que notre valeur comporte des guillemets simple, en plus d'un `:`, résultant en quelque chose d'invalide par la double présence de guillemets. Les guillemets simples qui encadre la valeur reste les mêmes, mais on en met deux d'affiler pour signifier des guillemets simples à l'intérieur.

```YAML
---
title: 'edition 1: l''arbre'
---
```

### Liquide et solide

Le site web est divisé en deux parties:
* partie **liquide**, ce qui arrive et est en cours dans le calendrier.
* partie **solide**, ce qui est passé et est rangé dans la bibliothèque.

**Les pages sont par défaut solides**, car elles constituent la majorité du site, donc si on ne précise pas cette clé, la page est dans la partie solide.
Seul les événements de l'année en cours, passé ou à venir, sont potentiellement liquides.

Pour faire apparaître un événement dans le calendrier (partie liquide), on lui ajoute le clé `state:liquid`, comme ceci.

```YAML
---
title: "Producers' Academy 2021"
state: liquid
---
```

Une fois l'événement passé, on retire cette ligne pour le faire passer dans la bibliothèque (partie solide).

---

## Corps en Markdown

La partie principale de chaque fichier est écrite en Markdown. Elle commence juste après la _frontmatter_ en YAML qui se termine par `---`.

> Markdown est un langage de balisage léger, [...] dans le but d'offrir une syntaxe facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières.

Il sert à structurer (baliser) un texte, en indiquant qu'est-ce qui est par exemple _un titre (de niveau 1)_, _un sous-titre (de niveau 2)_, _une liste_, _un lien_, etc; et donc à induire une certaine mise-en-forme. Par example, ce document-ci est lui-même rédigé en Markdown.

[Zettlr](https://www.zettlr.com/) est un logiciel libre qui permet de prendre des notes en markdown encore plus facilement car il suggère la syntaxe automatiquement (Voir l'autre document `HOWTO-Setup`).

Dans tout les cas voici un [résumé de la syntaxe Mardown](https://www.markdownguide.org/cheat-sheet/).

### Images

Les images à même le texte d'une page, par exemple après un titre, entre deux paragraphes, etc; doivent-être inserée en markdown.

```MARDKDOWN
![alt text](lien relatif au fichier d'image)
```

Premièrement il faut placer l'image dans un sous-dossier du dossier `images/` à la racine. Par exemple pour un évènement, on pourrait la mettre dans l'année correspondante.

```
images/
└── event/
    └── 2022/
        |── photo1.jpg
        └── photo2.jpg
```

Pour inserer l'image, le plus simple est de `drag & drop` l'image depuis un dossier vers _Zettlr_, et la syntaxe, ainsi que le _lien relatif_ s'écrira tout seul.

On peu ensuite cliquer sur l'image pour l'éditer si besoin.

Le `alt text` contenu dans la syntaxe de l'image, est une description écrite de celle-ci. Il a plusieurs usages mais joue notament un rôle essentiel dans l'accessibilités d'un site web. 
Il permet au personne qui sont aveugles, ou ont une vision basse, ou d'autres handicaps cognitifs, d'acceder au contenu de l'image via des technologies tierces: par exemple via une extension de leur navigateur, un système qui lis le contenu de l'écran, etc.

---

## Event

Les évènements ont des méta-donnés ainsi qu'une structure spécifique.

Les évènements (qu'ils soient passés ou à venir) sont dans le dossier `event/`, ensuite sous-divisé par années.

```
...
└── event/
    |── 2021/
    └── 2022/
```

**Cette première structure est obligatoire, ensuite chaque année peut-être divisé comme on veut**, par example en type d'évenements. Cette sous-division est de l'ordre de l'organisation interne de l'éditeur·ice, et relève de ses préférences.

### Cover d'event

Pour joindre un image à un événement, on utilise la méta-donnée `event_cover`, cette clé doit contenir **le nom du fichier de cover avec son extension** (soit `jpg` ou `png`).

Le fichier de cover doit se trouver dans le dossier `images/event/` suivit de l'année.

```YAML
event_cover: photo_de_cover.jpg
```

```
images/
└── event/
    └── 2022/
        └── photo_de_cover.jpg
```

### Dates des évenements

L'écriture des dates d'un événement se fait en utilisant la clé `event_dates`, qui consiste en une liste de date.

Voici un exemple sur 2 dates, pour un événement qui fini plus tôt le deuxième jour:

```YAML
event_dates:
- date: '15/01/22' 
  time: '10h > 18h'
- date: '18/01/22' 
  time: '10h > 15h'
```

Chaque date commence avec un `-` pour signifier un nouvel élément de la liste et possède plusieurs clés: 
* `date`: un jour, dans le format `dd/mm/yy` (deux chiffres pour respectivement chaque jour, mois et année; séparé par un `/`)
* `time`: un horaire.
* `free`: une info pratique supplémentaire sur cette date.

Chacunes de ces clés doivent être **indenté** avec des espaces pour arriver au même niveau que le précédent. 
Une date forme donc un bloc aligné qui commence par un `-`.

Les clés `date` et `time` peuvent être écrits comme des périodes en faisant suivre deux jours ou deux heures séparées de ` > ` (avec un espace de chaque côté).

Voici un exemple commenté qui montre différentes possibilités.

```YAML
event_dates:
# une date
- date: '15/01/22'
# une date avec une heure de début et de fin
- date: '15/01/22' 
  time: '10h > 18h'
# une période délimitée par une date de début et de fin
- date: '12/02/22 > 13/02/22'
# une période délimitée par une date de début et de fin
# et avec un horaire identique pour chaque jour sur cette période
- date: '19/03/22 > 20/03/22'
  time: '10h > 18h'
# une date avec le champ libre utilisé
- date: '15/01/22'
  free: 'walk outside'
```

On peut ainsi se faire suivre plusieurs périodes, date ou horaires en fonction de l'événement.

La clé `free` permet de rajouter une info complémentaire aux dates, que l'on écrit comme on veut.

Un évènement doit avoir au moins une date pour être annoncé et apparaître sur le calendrier, comme celui-ci fonctionne par chronologie des évènements c'est indispensable.

### Afficher les dates complètes sur le calendrier

Su la page calendrier, les dates sont affichées comme un résumé sans les heures qui le montre comme une période de la première à la dernière date. Pour un festival d'une semaine, cela conviens, mais il est possible que ce résumé ne traduise pas vraiment la temporalité de l'évènement: par example si celui-ci à une date en janvier et une date en août sans aucune date entre, cela paraîtrais bizarre de l'afficher comme étant de janvier à août.

Une clé supplémentaire `event_dates_full` peut-être ajoutée.
Si celle-ci est présente l'entièreté des dates sera montrée sur la page calendrier.

```YAML
event_dates_full: true
``` 

### Autre méta-donnée d'évenements

**Chacune de ces clés est facultatives.**

Toute les valeurs qui sont des liste de mots sont simplement séparé par des virgules.

Pour éviter qu'une personne soit sur plusieurs noms différents, ou qu'on retrouve des formes plurielles comme différentes dans les tags, ou des nom de catégories différents alors que l'activitée est similaire, il faut faire un peu attention à la nomenclature utilisée.

#### Catégorie

Type d'activité, en quoi consiste l'évenement, ce qu'on y fait. Le rôle de la catégorie d'évèvement à été défini comme le plus factuel possible, il est donc important de garder une nomenclature claire et minimal dans le choix des catégories. Les catégorie initiallement décidés sont (en français): `workshop`, `discussion`, `ballade`, `performance`.
On peut tout de même en rajouter ci nécéssaire, par exemple: `film`.

```YAML
type: 'workshop, walk'
```

#### Les tags 

Contrairement à la catégorie sont super libre. C'est des champs thématiques pour présenter la page comme une suite de mots. Celle-ci permet des recherches et des classements.

```YAML
tags: 'feral, ephemeral, rocks, food'
```

#### L'adresse 

Peut ou être une simple adresse, ou être une liste de couple `'nom' : 'addresse'`. Cette liste peut contenir un seul élément, pour séparer le nom de l'adresse et son adresse.

```YAML
# adresse simple
event_address: 'Rue de Flandre 46, 1000 Bruxelles'
# adresse nommée
event_address: 
- 'La Bellone' : 'Rue de Flandre 46 - 1000 Bruxelles. Ferme Urbaine le Début des Haricots - Mariënborre 40A - 1120 Neder-Over-Heembeek.'
- 'Open Akker' : 'en face du n°253, Herdebeekstraat - Sint-Anna-Pede - 1701 Dilbeek'
```

#### Agents / artistes / intervenant·es / autheur·ices

Une liste des personnes au noeud de l'évèvements. 
Cette collection de nom ne doit pas être exhaustive, mais est libre quand à son interpretation. Elle permet des recherches et classements.

```YAML
authors: 'Doriane Timmermans, Sarah Magnan, Cifas, OSP'
```

#### Short

Courte description de l'évenement

```YAML
event_short: The Producers' Academy is a dedicated time to  reflect, think, co-learn, encounter, and provoke the practice of the  producer. With open minds and open hearts, integrity and fun, this 4-day workshop is willing to dig into the current urgencies and problematics of producing work as well as being a producer.
```

#### Accès & Formulaire

Clé libre pour préciser le type d'accès.
Typiquement: `Sur candidature` ou `Sur inscription`, ou alors la clé est absente pour ne rien préciser ce qui sous-entend accès libre.

Celle ci peut-être jointe d'un formulaire [framaform](https://framaforms.org/abc/en/).
La documentation des formulaire ce trouve dans cette [exemple](https://docs.framasoft.org/fr/framaforms/exemple-d-utilisation.html). Une fois le formulaire créé sur le compte du cifas, on l'intègre en copiant/collant son lien dans une nouvelle clé.

```YAML
event_access: 'Sur candidature'
event_form: "https://framaforms.org/residence-secondaire-1660056905"
```

<!-- #### ~~Public~~

**Cette clé à été retirée après plusieurs discussion, on prefère finalement ne pas séparé le public pro ou non dans la communication.**

Soit 'pro', soit absent (pour dire 'all'). -->
<!-- note: ce n'est pas équivalent au dossiers learning / feral (on peut avoir du 'pro' dans les deux) -->
<!-- note: un event 'pro' peut-être en accès libre ou sur inscription -->

<!-- ```YAML
event_public: 'pro'
``` -->


### Groupes d'évènements

Certain évènements sont en fait des **groupes d'évènements**.
C'est le cas par exemple du festival annuel _Feral_.

Cela veut dire que:
* le groupe d'évènements est décomposé en une série de page différentes par sous-évènements.
* seule la page global du groupe d'évènement apparaît sur le calendrier.
* le reste des pages est proposé comme un _programe_ qui s'accède par la première.

Cette découpe est souvent dicté par le faite que les différents sous-évènements du groupe ont des modalités pratiques ou une temporalité différente. Cependant, la découpe en groupe d'évènement ou non est un outil d'édition flexible, c'est à l'éditeur·ice de décider des réglès précises de sont utilisation.

* **Les événements groupés doivent être placés dans un même dossier.**
* La page qui réprésente le groupe dans son ensemble, doit avoir `index.md` comme nom de fichier. 
* Les événements singuliers qui le compose se placent simplement dans le même dossier.

Dans cet example, `feral festival` est un groupe d'évènement, et la page qui apparaîtra directement sur le calendrier est `feral festival/index.md`. Les autres font partie de son _programe_.

```
2022/
├── un_show.md
├── un_workshop.md
├── une_marche.md
└── feral festival/
    ├── index.md
    ├── event_du_festival.md
    └── autre_event_du_festival.md
```

Les évenements singuliers qui ne sont pas rattachés à un événement groupé se placent où l'on veut.


---

## Traductions

L'arborescence est divisé en deux sous-dossier à la racine en fonction de la langue, qui se suivent d'exactement la même structure, comme ceci:

```
├── fr/
│   └── event/
│       └── .../
│           ├── event_1.md
│           └── event_2.md
└── en/
    └── event/
        └── .../
            ├── event_1.md
            └── event_2.md
```

Afin que deux _events/articles/pages_ soient détéctés comme des traductions l'un de l'autre, il faut qu'ils aient exactement le même chemin relativement à leur premier dossier qui détermine la langue.

**Il faut donc s'assurer que le nom du fichier (avec l'extension `.md`) soit le même pour la version `fr` et `en`.**

La langue par défaut est `fr`, ce qui veut dire que les _events/articles/pages_ en `en` **n'ont pas besoin d'avoir tout leurs clés de méta-donnés définis!**
Dans le cas ou un clé est manquant, celle déclarée dans la version original en `fr` est utilisé.

Cette fonctionnalité permet que, en cas de besoin d'effectuer un changement, par exemple sur les `event_dates`, la `event_cover`, ou de passer un évenement en solide en enlevant la ligne `state: liquid`, on peut simplement le faire dans la version `fr`.
Pour cela **il ne faut déclarer dans la version `en` que les clés qui sont des traductions, ou sont différents de l'original.**


## Fichier de configuration

Ce fichier permet de gérer des réglagles généraux du site. Il se trouve dans `data/config.yaml`.

Il se compose de deux dictionnaire, un `fr` et en `en`, et permet d'éditer de la nomenclature générale ou des textes globaux dans les deux langues.