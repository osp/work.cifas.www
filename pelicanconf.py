#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from markdown import Markdown, markdown
import itertools
import os
import yaml

AUTHOR = 'Cifas'
SITENAME = 'Cifas'
SHORT = 'Cifas is a place of learning and experimentation for performing art in the city and its fringes, based in Brussels.'
SITEURL = ''
COVER = 'https://cifas.be/theme/card.png'

PATH = 'content'
THEME = 'theme'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True


# IMPORT THE CONFIG.YAML
# ==============================================================================

with open("content/data/config.yaml", "r") as stream:
    try:
        YAML_CONFIG =  yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)


# ARCHITECTURE
# ==============================================================================

STATIC_PATHS = ['images', 'data', 'shapes', 'pdf']
STATIC_CHECK_IF_MODIFIED = True

# copy the static files to the same path
STATIC_SAVE_AS = '{path}'
STATIC_URL = '{path}'

# those 2 variables should not exist but anyways it works like that
# https://github.com/getpelican/pelican/issues/3032
STATIC_LANG_SAVE_AS = '{path}'
STATIC_LANG_URL = '{path}'

# article location follow the whole nested path of categories
# we do not have to add the lang in the url because it's part of more-category (see plugins)
# ARTICLE_SAVE_AS = ARTICLE_LANG_SAVE_AS = '{lang}/{category}/{slug}.html'
# ARTICLE_URL = ARTICLE_LANG_URL = '/{lang}/{category}/{slug}.html'
# PAGE_SAVE_AS = PAGE_LANG_SAVE_AS = '{lang}/pages/{slug}.html'
# PAGE_URL = PAGE_LANG_URL = '/{lang}/pages/{slug}.html'

# all the folders in which we put articles
ARTICLE_PATHS = ['event', 'cifasotheque', 'newsletter']
# all the folders in which we put pages
PAGE_PATHS = ['page']

# a regex that extract any metadata from the whole source_path
# the () is for capturing group, the ?P<meta_data_name> give it to a pelican metadata
# first folder is lang metadata, others are category, category + filename forms the translation id
# V0 - folder at root for lang was not really practical
# PATH_METADATA = '^(?P<lang>.+?)\/(?P<transid>(?P<category>.*)\/.*)'
# V.01 - lang inside filename
PATH_METADATA = '^(?P<transid>(?P<category>.*)\/.*)_(?P<lang>fr|en).md'
# see https://docs.getpelican.com/en/latest/settings.html and search for PATH_METADATA (super interesting stuff!)

# we need the slug to be unique in order to use .js later on their ids
# so no basename because multiple file are named index
SLUGIFY_SOURCE = 'title'

# default is 'slug', meaning for two article to be identified as translation
# we need to manually setup the same slug metadata for both of them
# my alternative use the path location:
# articles are translation if they have the same relative path to their lang folder with same filename
ARTICLE_TRANSLATION_ID = ['transid']
PAGE_TRANSLATION_ID = ['transid']

# disable the generation of those default html files
AUTHOR_SAVE_AS = ''
TAG_SAVE_AS = ''
CATEGORY_SAVE_AS = ''

# default date is file meta-data
DEFAULT_DATE = 'fs'
DEFAULT_DATE_FORMAT = '%d/%B/%Y'

# MD EXTENSION
# ==============================================================================

import yafg
# https://pypi.org/project/yafg/

# from markdown_figcap import FigCapExtension
# https://github.com/funk1d/markdown-figcap

# from zettlr2pelican import Zettlr2PelicanExtension

MARKDOWN = {
    'extensions': [
    ],
    'extension_configs': {
        'yafg': {
            'stripTitle': 'False',
        },
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {'toc_depth': '2'},
    },
    'output_format': 'html5',
}

# for officially and third party extensions:
# https://python-markdown.github.io/extensions/


# PLUGINS
# ==============================================================================

import logging
LOG_FILTER = [(logging.WARN, 'Empty alt attribute for image %s in %s')]

PLUGIN_PATHS = ['plugins']
PLUGINS = ['i18n_subsites', 'extract_toc', 'more_categories', 'yaml_metadata', 'data_files', 
           'cifas_datesranges', 'auto_attach', 'image_process', 'links_genres']

# --- I18N Sub
# https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites
# what this plugin does is group article by lang attribute and generate a subsite for each of them
# each subsite as DEFAULT_LANG = lang, so the article list is the translated one (meaning article are not translation of each others anymore)
# the switch of DEFAULT_LANG for each subsite also implies that the original article become translation when switching

# only article in this lang will be listed by default
DEFAULT_LANG = 'fr'
# custom setting that is only there to get the 'real default lang' in other plugins 
MAIN_LANG = 'fr'

# do not generate main site (article and pages)
# IGNORE_FILES = ['*.md']
ARTICLE_SAVE_AS = ARTICLE_LANG_SAVE_AS = ''
ARTICLE_URL = ARTICLE_LANG_URL = ''
PAGE_SAVE_AS = PAGE_LANG_SAVE_AS = ''
PAGE_URL = PAGE_LANG_URL = ''
# do not generate direct template
INDEX_SAVE_AS = ''
LIQUID_SAVE_AS = ''
VITRINE_SAVE_AS = ''
TAG_SAVE_AS = ''
TAGS_SAVE_AS = ''
AUTHORS_SAVE_AS = ''

DIRECT_TEMPLATES = ['index', 'liquid', 'vitrine', 'archives', 'tags', 'authors']

I18N_UNTRANSLATED_ARTICLES = 'keep'
I18N_LINK_DIRS = ['images']

LIQUID_HOME_EN = YAML_CONFIG['en']['liquid'].lower()+'.html'
SOLID_HOME_EN = 'cifasotheque/' + YAML_CONFIG['cifasotheque']['main']['fr']['name'].lower()+'.html'
LIQUID_HOME_FR = YAML_CONFIG['fr']['liquid'].lower()+'.html'
SOLID_HOME_FR = 'cifasotheque/' + YAML_CONFIG['cifasotheque']['main']['fr']['name'].lower()+'.html'

BASIC_SUBSITES = {
    'SITENAME': 'Cifas',
    'ARTICLE_SAVE_AS': '{category}/{slug}.html',
    'ARTICLE_URL': '/{lang}/{category}/{slug}.html',
    'PAGE_SAVE_AS' :'pages/{slug}.html',
    'PAGE_URL' : '/{lang}/pages/{slug}.html',
    'CATEGORY_SAVE_AS' : '/{slug}.html',
    'INDEX_SAVE_AS' : 'index.html',
    'IGNORE_FILES' : [],
}

I18N_SUBSITES = {
    'en': {
        # direct templates
        'LIQUID_SAVE_AS' : LIQUID_HOME_EN,
        'VITRINE_SAVE_AS' : SOLID_HOME_EN,
        'TAGS_SAVE_AS' : YAML_CONFIG['en']['desire_paths'][0]['name'].lower()+'.html',
        'AUTHORS_SAVE_AS': YAML_CONFIG['en']['desire_paths'][1]['name'].lower()+'.html',
        'DEBUG_SAVE_AS' : '',
        # menu item
        'MENUITEMS' : [
            (YAML_CONFIG['en']['home'], '', 'home'),
            (YAML_CONFIG['en']['liquid'] , LIQUID_HOME_EN, 'liquid'),
            (YAML_CONFIG['en']['solid'] , SOLID_HOME_EN, 'solid')
        ],
        'LOCALE' : 'en_US.utf8',
    },
    'fr': {
        # direct templates
        'LIQUID_SAVE_AS' : LIQUID_HOME_FR,
        'VITRINE_SAVE_AS' : SOLID_HOME_FR,
        'TAGS_SAVE_AS' : YAML_CONFIG['fr']['desire_paths'][0]['name'].lower()+'.html',
        'AUTHORS_SAVE_AS': YAML_CONFIG['fr']['desire_paths'][1]['name'].lower()+'.html',
        'DEBUG_SAVE_AS' : '',
        # menu item
        'MENUITEMS' : [
            (YAML_CONFIG['fr']['home'], '', 'home'),
            (YAML_CONFIG['fr']['liquid'] , LIQUID_HOME_FR, 'liquid'),
            (YAML_CONFIG['fr']['solid'] , SOLID_HOME_FR, 'solid')
        ],
        'LOCALE' : 'fr_FR.utf8',
    }
}

I18N_SUBSITES['en'] = {**I18N_SUBSITES['en'], **BASIC_SUBSITES}
I18N_SUBSITES['fr'] = {**I18N_SUBSITES['fr'], **BASIC_SUBSITES}


# --- MORE CATEGORIES
# let's really use the fact that the CMS (nextcloud) is a nested structure :)
# allow nested categories
# https://github.com/pelican-plugins/more-categories

# the first of those category is the type of content it is:
# events, contributions, klaxon, etc. 

# --- YAML METADATA
# this is used for event_date allowing us to precise the date of an event as a dictionnary in the metadata
# Note: has been edited line 56 so that tags doesn't need to be noted a yaml list, but can be comma list

# --- data_files (from: https://github.com/LucasVanHaaren/pelican-data-files, edited with custom added YAML support)
# by parse the json and yaml file in the data/ directory and add them tp the context
# as object with name "DATA_[filename]"
# DATA_FILES_DIR = "config/"


# --- image_process (from: https://github.com/pelican-plugins/image-process)
# note: we can add the class manually in the html template (and not in the markdown)
IMAGE_PROCESS = {
    # reminder on srctset
    # https://www.youtube.com/watch?v=2QYpkrX2N48&ab_channel=KevinPowell
    # our gallery <img> html element varies between
    # 300 and 500 browser pixels width
    # so on 1x devices we give it a 500px image to be sure
    "calendar": {
        "type": "responsive-image",
        "srcset": [
            ("1x", ["scale_in 600 600 True"]),
            ("2x", ["scale_in 1200 1200 True"])
        ],
        "default": "1x"
    },
    "cover": {
        "type": "responsive-image",
        "srcset": [
            ("1x", ["scale_in 750 750 True"]),
            ("2x", ["scale_in 1500 1500 True"])
        ],
        "default": "1x"
    },
    # note that this class is added through the auto-attach plugin :)
    "float": {
        "type": "responsive-image",
        "srcset": [
            ("1x", ["scale_in 450 450 True"]),
            ("2x", ["scale_in 900 900 True"])
        ],
        "default": "1x"
    }
}


# HOME SHAPES
# ==============================================================================

import json
import shutil

possible_extensions = ['.svg']
forwarded_from = 'content/shapes/'
forwarded_to = 'theme/templates/shapes/'

def make_shapes_list(subfolder):
    path = forwarded_from + subfolder
    urls = []
    for root, dirs, files in os.walk(path):
        for name in files:
            (basename, ext) = os.path.splitext(name)
            if ext in possible_extensions:
                urls.append(name)
                shutil.copyfile(forwarded_from + subfolder + name, forwarded_to + subfolder + name)
    return urls


LIQUID_SHAPES = make_shapes_list('liquid/');
SOLID_SHAPES  = make_shapes_list('solid/');


# FILTER
# ==============================================================================

import datetime
from dateutil.parser import *

valid_indexes = ['index_fr.md', 'index_en.md']

def md_parse(value):
    # to parse yaml field as markdown
    return markdown(value, extensions=MARKDOWN['extensions'], extension_configs=MARKDOWN['extension_configs'])

def get_index(list):
    # get the program from a list of events
    for art in list:
        if os.path.basename(art.source_path) in valid_indexes:
            return art

def remove_index(list):
    # remove the program from a list of events
    return [
        art for art in list 
        if os.path.basename(art.source_path) not in valid_indexes
    ]

def has_index(list):
    # check if there is an index in this list
    return [art for art in list if os.path.basename(art.source_path) in valid_indexes]

def is_index(art):
    # test if an event is a program
    return os.path.basename(art.source_path) in valid_indexes

def main_events(list):
    # list are article object
    # return only the events that are not subevents of another event
    return [
        art for art in list 
        if not [f for f in os.listdir(os.path.dirname(art.source_path)) if f in valid_indexes]
        or is_index(art)
    ]

def sort_by_article_count(list):
    return sorted(list, key= lambda x: len(x[1]), reverse=True )

def sort_by_min_date(events):
    return sorted(events, key= lambda x: x.event_dates['min']['d'], reverse=True )

def has_cat(articles, cat):
    # return only articles that have a specific category amongst
    # they categories ancestors
    return [article for article in articles if str(cat).lower() in str(article.category).lower()]

def get_cat(categories, name):
    return [category for category in categories if str(category[0].shortname).lower() in str(name).lower()]

def url_2_lang(article, lang):
    # return only articles that have a specific category amongst
    # they categories ancestors
    for t in article.translations:
        print(t.lang)
    return "/".join([lang] + article.url.split('/')[2::])

def to_date(date_str):
    date = parse(date_str, dayfirst=True)
    return date

def get_vitrine(articles, category, range):
    valid_articles = has_cat(articles, category)
    vitrine_recent = sorted(valid_articles, key= lambda x: (x.priority if hasattr(x, 'priority') else 0, x.date), reverse = True)
    vitrine = vitrine_recent[:range]
    return vitrine

def get_common_tags(articles):
    common_tags = {}
    for article in articles:
        for tag in article.tags:
            if tag not in common_tags:
                common_tags[tag] = 1
            else:
                common_tags[tag] += 1
    common_tags = [tag for tag, count in common_tags.items() if count > 1]
    return common_tags

def config_sort(categories, DATA_CONFIG):
    categories = sorted(categories, key= lambda x: DATA_CONFIG['cifasotheque'][x.shortname]['order'])
    return categories


JINJA_FILTERS = {
    'md_parse': md_parse,
    'get_index': get_index,
    'remove_index': remove_index,
    'has_index': has_index,
    'is_index': is_index,
    'main_events': main_events,
    'sort_by_article_count': sort_by_article_count,
    'sort_by_min_date': sort_by_min_date,
    'has_cat': has_cat,
    'get_cat' : get_cat,
    'url_2_lang': url_2_lang,
    'to_date': to_date,
    'get_vitrine': get_vitrine,
    'get_common_tags': get_common_tags,
    'config_sort': config_sort,
}