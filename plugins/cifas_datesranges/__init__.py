from pelican import signals
from dateutil.parser import *
import logging
log = logging.getLogger(__name__)


# DATES RANGES
# ==============================================================================


def parse_date(date_str, article):
    try:
        date = parse(date_str, dayfirst=True)
    except ParserError as e:
        log.error("DATE ERROR (bad parsing) in {p}\n-> {err} ".format(p = article.relative_source_path, err = str(e)))
    return date

def compute_date_ranges(article):

    # local = ''
    # lang = article.settings.get('DEFAULT_LANG', '')
    # if lang == 'fr': 
    #     local = 'fr_FR.UTF-8'
    # elif lang == 'en': 
    #     local = 'en_US.UTF-8'

    # print(local)
    # locale.setlocale(locale.LC_ALL, local)

    event_dates = article.metadata.get('event_dates', None)
    log.debug('dates for {a} ({l})'.format(l = article.lang, a = article.title))

    if event_dates:

        log.debug('-- found event_dates to sort')

        valid_dates2sort = []
        valid_dates = []
        has_time = False

        # parse every element as a date object
        for date_range in event_dates:

            if 'date' in date_range:

                # split date data
                d_range = date_range['date'].split('>')

                # split time data if there is
                if 'time' in date_range:
                    has_time = True
                    time_range = date_range['time'].split('>')

                # put the time range in both of (max 2) dates
                date_range = [ {'d': date , 't': time_range} if 'time' in date_range else {'d': date} for date in d_range]

                # parsing both date and time
                for d in date_range:
                    to_parse = d['d'] + ' ' + d['t'][0] if 't' in d else d['d']
                    d['d'] = parse_date(to_parse, article)
                    if 't' in d:
                        d['t'] = [parse_date(time, article) for time in d['t']]

                # thus we have something like this:
                # range = [{
                #   d: d0+t0,
                #   t: [t0, t1]
                # },{
                #   d: d1+t0,
                #   t: [t0, t1]
                # }]

                # add for sorting
                valid_dates2sort = valid_dates2sort + date_range
                valid_dates = valid_dates + [date_range]


        valid_dates2sort = sorted(valid_dates2sort, key=lambda date: date['d']) 

        if len(valid_dates2sort) == 0:
            log.error("DATE ERROR (no valid date) in {p}".format(p = article.relative_source_path))
        

        parsed_event_dates = {
            'full': valid_dates,
            'min': valid_dates2sort[0],
            'max': valid_dates2sort[-1],
            'has_time': has_time,
        }

        # save the parsed version
        article.event_dates = parsed_event_dates
        
        log.debug('-- min date: {d0}, max date: {d1}'.format(d0 =  article.event_dates['min']['d'], d1 = article.event_dates['max']['d']))
        # print(json.dumps(article.event_dates['min'], indent=4, sort_keys=True, default=str))
    else:
        log.error("NO DATE in {p}".format(p = article.relative_source_path))


def is_trace(generator, article):
    if 'traces.' in article.source_path:
        article.status = 'hidden'
        generator.traces.append(article)
        return True
    else:
        return False

def is_newletter(generator, article) :
    if 'newsletter' in article.category.name:
        article.status = 'draft'
        return True
    else:
        return False

def handle_traces(generator):

    trace_counter = 0
    for trace in generator.traces:

        # get the article with this traces and add it as a metadata
        # conditions are:
        # 1. filename == traces.filenames
        # 2. inside of same folder
        original_article = [a for a in generator.articles 
                            if ('traces.' + a.source_path.split('/')[-1] == trace.source_path.split('/')[-1]
                            and a.source_path.split('/')[-2] == trace.source_path.split('/')[-2] )]
        
        if len(original_article) > 0:
            if len(original_article) == 1:
                trace_counter = trace_counter + 1
                # print(trace.title, '->', original_article[0].title)

                oa = original_article[0]
                oa.trace = trace

            else:
                log.error("TRACE ERROR: {t} \multiple original article".format(t = trace.relative_source_path))
        else:
            log.error("TRACE ERROR: {t} \nwithout original article".format(t = trace.relative_source_path))

        # remove it from the official list of articles
        generator.articles.remove(trace)

    print(trace_counter, 'traces, each linked to exactly 1 event')


def inherit_articles(generator):

    def_lang = generator.settings.get('DEFAULT_LANG')
    main_lang = generator.settings.get('MAIN_LANG')
    is_main_lang = def_lang == main_lang

    print('\n------')
    print('| ' + def_lang + ' |')
    print('------')

    generator.traces = []

    # this is the articles list, meaning it only contains
    # article that are not translation, and the translation are sublist of them
    event_counter = 0
    cifasotheque_counter = 0
    news_counter = 0
    for article in generator.articles:

        bool_trace = is_trace(generator, article)
        if not bool_trace:

            bool_newletter = is_newletter(generator, article)
            if not bool_newletter:

                # if it's an event
                # compute the date range
                if "event" in article.category.name:
                    event_counter = event_counter + 1
                    compute_date_ranges(article)

                elif "cifasotheque" in article.category.name:
                    cifasotheque_counter = cifasotheque_counter + 1
                elif "newsletter" in article.category.name:
                    news_counter = news_counter + 1

            else:
                news_counter = news_counter + 1

    print(event_counter, 'events, each with a traduction fr-en')
    print(cifasotheque_counter, 'cifasotheque entries, each with a traduction fr-en')
    print(news_counter, 'newsletter, each with a traduction fr-en')
                    
    handle_traces(generator)


# REGISTER
# ==============================================================================

def register():
    # signals.article_generator_write_article.connect(date_ranges)

    # we have to parse the date of every article before generator a single one
    # because of the sub_events system
    signals.article_generator_pretaxonomy.connect(inherit_articles)