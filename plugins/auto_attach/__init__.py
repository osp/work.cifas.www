# -*- coding: utf8 -*-
from markdown.extensions import Extension
# import markdown.inlinepatterns as inlinepatterns
from markdown.inlinepatterns import LinkInlineProcessor
from markdown.inlinepatterns import ImageInlineProcessor
# import xml.etree.ElementTree as etree
from pelican import signals
import logging
import os.path

# in Zettlr the easiest to import in text image in markdown is to drag and drop them
# it is automatically written in this form
# ![filename.jpg](../../../images/event/2022/filename.jpg)
# however pelican ask for a different syntax in order to detect that an image is linked to an article
# ![filename.jpg]({static}../../../images/event/2022/filename.jpg)
# doing this will:
# * automatically includes the image in the website generation in the static folder 
#   (even the image itslef is not in the /images default static directory)
# * replace the img url to the post-generation one

# see:
# https://github.com/getpelican/pelican/issues/3099

# this is a doriane's edit of
# https://github.com/GiovanH/peliplugins#autoattach


# Pattern must not consume characters
ATTACH_IMAGE_RE = r'\!\[(?=[^\]]*?\]\()'
# ATTACH_IMAGE_RE = r'\!\[[^\]]*\]\(.*\)'
# ATTACH_LINK_RE = r'\[(?=[^\]]*?\]\(\./)'

class AttachImageInlineProcessor(ImageInlineProcessor):
    def handleMatch(self, m, data):
        # Process image as usual
        el, start, index = super().handleMatch(m, data)

        # Postprocessing
        if el is not None and el.get("src"):

            img_path = el.get("src")
            # print(img_path)

            # multiple case if url starting
            if img_path.startswith('http'):
                pass
                # logger.warning(
                #     "external img url: \n\"{u}\" \nin {f}"
                #     .format(u = img_path, f = file_path)
                # )
            elif img_path.startswith('{static}'):
                pass
                # logging.warning(
                #     "{{static}} img url: \n\"{u}\" \nin {f}"
                #     .format(u = img_path, f = file_path)
                # )
            elif img_path.startswith('C:'):
                pass
                # logging.error(
                #     "local absolute img url: \n\"{u}\" \nin {f}"
                #     .format(u = img_path, f = file_path)
                # )

            # relative url tracing back to 
            elif img_path.startswith(('..', '.\\', './')):

                new_img_path = img_path
                # print(type(new_img_path))

                # we don't make it {static} to not copy unecessary file
                # but we transform it into an absolut path - with the absolute being
                # relative to the website itself
                while new_img_path[0] in ['.', '/', '\\', '\x02', '4', '6', '\x03']:
                    new_img_path = new_img_path[1:]
                new_img_path = '/' + new_img_path.replace('\\', '/')

                # new_img_path = new_img_path.replace('\x0246\x03', '/.')
                # new_img_path = new_img_path.replace('\\', '/')
                # new_img_path = '{static}' + new_img_path

                # print(">", new_img_path)

                el.set("src", new_img_path)
                # logging.debug(f"Coercing src '{img_path}' to '{el.get('src')}'")

                el.set("class", "image-process-float")

            else:
                pass
                # logging.error(
                #     "wrong img url: \n{u} \nin {f}"
                #     .format(u = img_path, f = file_path)
                # )

        return el, start, index

# class AttachLinkInlineProcessor(LinkInlineProcessor):
#     def handleMatch(self, m, data):
#         # Process image as usual
#         el, start, index = super().handleMatch(m, data)

#         # Postprocessing
#         if el is not None and el.get("href"):
#             el_oldsrc = el.get("href")
#             el.set("href", "{attach}" + el_oldsrc)
#             logging.debug(f"Coercing href '{el_oldsrc}' to '{el.get('href')}'")

#         return el, start, index

class AutoAttachExtension(Extension):
    def extendMarkdown(self, md):
            md.inlinePatterns.register(AttachImageInlineProcessor(ATTACH_IMAGE_RE, md), 'attach_image', 170 + 1)
        # md.inlinePatterns.register(AttachLinkInlineProcessor(ATTACH_LINK_RE, md), 'attach_link', 160 + 1)

def pelican_init(pelican_object):
    pelican_object.settings['MARKDOWN'].setdefault('extensions', []).append(AutoAttachExtension())

def register():
    """Plugin registration"""
    signals.initialized.connect(pelican_init)
